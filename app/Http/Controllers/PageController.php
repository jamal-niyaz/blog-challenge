<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }

    public function getDetails()
    {
        return view('pages.detail');
    }

    public function getStories()
    {
        $articles = \App\Article::with('tags', 'author')->latest()->get();
        return view('pages.stories')->with('articles', $articles);
    }

    public function getTagBasedArticles(){
        return view('pages.detail');
    }

}
