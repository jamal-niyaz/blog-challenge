<?php

namespace App\Http\Controllers\Api;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('tags', 'author')->latest()->get();
        return response()->json(['data' => $articles], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:6|max:100',
            'short_content' => 'required|min:6|max:100',
            'long_content' => 'required|min:10|max:255',
        ]);

         $article = Article::create([
            'author_id' => $request->author_id,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'short_content' => $request->short_content,
            'long_content' => $request->long_content,
            'featured_image' => $request->featured_image,
            'article_image' => $request->article_image,
            'status' => 'published'
        ]);

         return response()->json($article, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Article $article)
    {
        $article = Article::with('tags', 'author')->find($article->id);
        return response()->json($article, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $article = Article::findOrfail($id);
        $article->author_id = 1;
        $article->title = $request->title;
        $article->short_content = $request->short_content;
        $article->long_content = $request->long_content;
        $article->status = $request->status;
        $article->updated_at = now();
        $article->save();

        return response()->json($article, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $article = Article::findOrfail($id);
        $article->delete();
        return response()->json($article, 204);
    }
}
