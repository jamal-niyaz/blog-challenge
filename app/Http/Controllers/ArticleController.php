<?php

namespace App\Http\Controllers;

use Image;
use App\Tag;
use App\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('tags', 'author')->latest()->get();
        return response()->json(['data' => $articles], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('pages.create')->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validate inputs
        $this->validate($request, [
            'title' => 'required|min:6|max:100',
            'short_content' => 'required|min:10|max:100',
            'long_content' => 'required|min:10|max:255',
            'featured_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // Image Process
        if ($files = $request->file('featured_image')) {
            // for save original image
            $ImageUpload = Image::make($files);
            $originalPath = public_path().'/uploads/images/';
            if (!file_exists($originalPath))
                mkdir($originalPath, 0755, true);
            $ImageUpload->save($originalPath.time().$files->getClientOriginalName());

            // for save thumbnail image
            $thumbnailPath = public_path().'/uploads/thumbnails/';
            if (!file_exists($thumbnailPath))
                mkdir($thumbnailPath, 0755, true);
            $ImageUpload->resize(500,300);
            $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());
        }

        $filename = time().$files->getClientOriginalName();

        // Save to DB
        $article = Article::create([
            'author_id' => auth()->user()->id,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'short_content' => $request->short_content,
            'long_content' => $request->long_content,
            'featured_image' => $filename ?? null,
            'article_image' => $filename ?? null,
            'status' => 'published'
        ]);

        //sync data to pivot table
        $article->tags()->sync($request->input('tags'), false);

        // Response
        if($request->ajax()){
            return response()->json($article, 201);
        }

        return redirect()->route('get.stories')->with('success','Article published successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $article = Article::with('tags', 'author')->find($article->id);
        return response()->json($article, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $article = Article::with('tags')->find($article->id);
        return view('pages.edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $article = Article::findOrfail($id);

        // Image Process
        if ( $request->hasFile('featured_image') ) {
            $files = $request->file('featured_image');
            // for save original image
            $ImageUpload = Image::make($files);
            $originalPath = public_path().'/uploads/images/';
            if (!file_exists($originalPath))
                mkdir($originalPath, 0755, true);
            $ImageUpload->save($originalPath.time().$files->getClientOriginalName());

            // for save thumbnail image
            $thumbnailPath = public_path().'/uploads/thumbnails/';
            if (!file_exists($thumbnailPath))
                mkdir($thumbnailPath, 0755, true);
            $ImageUpload->resize(500,300);
            $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());

            $filename = time().$files->getClientOriginalName();
        }

        if (!empty($filename)) {
            $article->featured_image = $filename ?? null;
            $article->article_image = $filename ?? null;
        }

        $article->author_id = auth()->user()->id;
        $article->title = $request->title;
        $article->short_content = $request->short_content;
        $article->long_content = $request->long_content;
        $article->status = 'published';
        $article->updated_at = now();
        $article->save();

        if (isset($request->tags)) {
            $article->tags()->sync($request->input('tags'), false);
        }else{
            $article->tags()->sync(array());
        }

        if($request->ajax()) {
            return response()->json($article, 200);
        }
        return redirect()->route('get.stories')->with('success','Article updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $article = Article::findOrfail($id);
        $article->delete();
        return response()->json($article, 204);
    }

    /**
     * Get Random Articles
     * @return JsonResponse
     */
    public function getRandomFeatured()
    {
        $articles = Article::with('author')->inRandomOrder()->take(5)->get();
        return response()->json(['data' => $articles], 200);
    }

    /**
     * Get Tags Related Articles
     * @return JSON
     */
    public function getTagArticles($id)
    {
        $tags = Tag::find($id);
        if(request()->ajax()) {
            return response()->json($tags->articles, 200);
        }
        return view('pages.tag-articles');
    }
}
