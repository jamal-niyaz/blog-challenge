<?php

namespace App;

use App\Tag;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];

    public function getFeaturedImageAttribute($value) {
        if( $value == "https://via.placeholder.com/500x300" ) {
            return $value;
        }
        return asset('uploads/thumbnails/'.$value);
    }

    public function author(){
        return $this->belongsTo(User::class);
    }

    public function tags(){
    	return $this->belongsToMany(Tag::class);
    }
}
