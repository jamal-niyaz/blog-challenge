<?php

namespace App;

use App\Article;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    protected $hidden = ['pivot'];

    public function articles(){
    	return $this->belongsToMany(Article::class);
    }
    
}
