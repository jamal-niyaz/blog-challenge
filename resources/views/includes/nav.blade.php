<div id="app" class="font-sans antialiased text-black">
    <div id="nav" class="bg-grey-white ">
        <div class="container mx-auto flex items-center justify-between px-10 py-4">
            <div>
                <span><a class="text-white" href="/"><img src="{{ asset('assets/md-logo.png') }}" height="50px" alt=""></a></span>
            </div>
            <div>
                @guest
                <a href="javascript:void(0)" class="text-grey-dark hover:text-grey-darkest no-underline">Become a member</a>
                <a href="/login" class="text-grey-dark hover:text-grey-darkest no-underline px-6">Sign in</a>
                <a href="/register" class="bg-transparent text-grey-dark hover:text-grey-darkest font-semibold py-2 px-4 border border-green rounded no-underline">
                    Get Started
                </a>
                @else
                <a href="javascript:void(0)" class="text-blue hover:text-grey-dark no-underline px-2"><i class="fa fa-bell-o" aria-hidden="true"></i></a>
                <a href="{{ route('get.stories') }}" class="text-blue hover:text-grey-dark no-underline px-2">Stories</a>
                <a href="{{ route('home') }}" class="text-blue hover:text-grey-dark no-underline px-2">Profile</a>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                    class="bg-transparent hover:bg-grey-dark text-blue-dark font-semibold hover:text-white py-1 px-2 border border-blue hover:border-transparent rounded no-underline">
                    Sign out
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @endguest
            </div>
        </div>
    </div>
</div>