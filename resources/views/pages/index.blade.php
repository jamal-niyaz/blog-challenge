@extends('layouts.medium')

@section('content')
<div class="container mx-auto">
    <div class="flex flex-wrap mx-2">

        <!-- Articles List Content -->
        <div class="w-full lg:w-3/4 px-4 mb-10">
            <blog-list></blog-list>
        </div>

        <!-- Sidebar -->
        <div class="w-full lg:w-1/4 px-4 mb-8">
            <div class="px-4 mb-8">
                <h2 class="mb-3">Popular on Medium</h2>
                <div class="border-b-2 border-grey-light mb-1"></div>
                <featured-articles></featured-articles>
            </div>
        </div>

    </div>
</div>
@endsection
