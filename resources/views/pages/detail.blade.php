@extends('layouts.medium')

@section('content')
    <div class="container mx-auto">
        {{-- Article Detail --}}
        <blog-detail></blog-detail>
    </div>
@endsection
