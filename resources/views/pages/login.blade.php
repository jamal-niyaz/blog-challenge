@extends('layouts.medium')

@section('content')
    <div class="container mx-auto">
        <div class="flex mb-4">
            <div class="w-full text-grey-darker px-12 py-2 m-2 mt-10">
              <form method="POST" action="{{ route('login') }}" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                @csrf
                <div class="mb-4">
                  <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
                    Email
                  </label>
                  <input name="email" required class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username">
                  @error('email')
                    <p class="text-red text-xs italic">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-6">
                  <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    Password
                  </label>
                  <input name="password" required class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="******************">
                  @error('password')
                    <p class="text-red text-xs italic">{{ $message }}</p>
                  @enderror
                </div>
                <div class="flex items-center justify-between">
                  <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Sign In
                  </button>
                  {{-- <a class="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker" href="#">
                    Forgot Password?
                  </a> --}}
                </div>
              </form>
              <p class="text-center text-grey text-xs">
                ©2019 Blog Test @ Mindvalley. All rights reserved.
              </p>
            </div>
        </div>
    </div>
@endsection