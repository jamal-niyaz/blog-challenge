@extends('layouts.medium')

@section('content')
    <div class="container mx-auto">
        <div class="w-2/3 max-w-sm p-8 animated fadeIn">
              <div class="p-2">
                  <div class="border shadow">
                      <div class="border-t-4 -mt-1 border-green w-2/4 flex flex-wrap justify-center pb-8 bg-green-lightest">
                          <h3 class="w-full p-4 text-grey-darkest">Hi, {{ '@'.auth()->user()->name }}</h3>
                          <a href="{{ route('articles.create') }}" class="bg-green-dark hover:bg-green-light text-white font-semibold py-4 px-4 border border-blue hover:border-transparent rounded no-underline">
                              New Article
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
@endsection
