@extends('layouts.medium')

@section('content')
    <div class="container flex mx-auto">
        <div class="w-full px-6 animated fadeIn">
            @if(session('success'))
              <div class="items-center bg-blue text-white text-sm font-bold px-4 py-3" role="alert">
                <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
                <p>{{ session('success') }}</p>
              </div>
            @endif
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Title</th>
                  <th scope="col">Excerpt</th>
                  <th scope="col">Tags</th>
                  <th scope="col">Author</th>
                  <th scope="col">Created</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($articles as $key => $article)
                <tr>
                  <th scope="row">{{ $key + 1 }}</th>
                  <td>{{ $article->title }}</td>
                  <td>{{ $article->short_content }}</td>
                  <td>@foreach($article->tags as $tag) <span>{{ $tag->title }}</span> @endforeach</td>
                  <td>{{ $article->author->name }}</td>
                  <td>{{ $article->created_at->diffForHumans() }}</td>
                  <td>
                    <a href="{{ route('articles.edit', $article->id) }}" class="bg-blue hover:bg-blue text-white font-semibold py-2 px-4 rounded-full">Edit</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
@endsection
