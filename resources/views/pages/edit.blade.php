@extends('layouts.medium')

@section('extra-css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
@endsection

@section('content')
    <div class="container mx-auto">
        <div class="flex mb-4">
            <div class="w-full text-grey-darker px-12 py-2 m-2 mt-6">
                @if(count($errors->all()) > 0)
                  <div class="bg-red border-l-4 border-red text-white p-4 mb-5" role="alert">
                    <p class="font-normal text-white">Please fix errors</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div>
                @endif

              <form method="POST" action="{{ route('articles.update', $article->id) }}" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mb-4">
                  <label class="block text-grey-darker text-sm font-bold mb-2" for="title">
                    Title
                  </label>
                  <input name="title" required class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="username" value="{{ $article->title }}" type="text" placeholder="Title">
                  @error('title')
                    <p class="text-red text-xs italic">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-4">
                  <label class="block text-grey-darker text-sm font-bold mb-2" for="excerpt">
                    Excerpt
                  </label>
                  <textarea required placeholder="Short Content" name="short_content" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="short_content" cols="3" rows="2">{{ $article->short_content }}</textarea>
                  @error('short_content')
                    <p class="text-red text-xs italic">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-4">
                  <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                    Description
                  </label>
                  <textarea required name="long_content" placeholder="Description" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="long_content" cols="3" rows="5">{{ $article->long_content }}</textarea>
                  @error('long_content')
                    <p class="text-red text-xs italic">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-grey-darker text-sm font-bold mb-3" for="email">
                      Tags
                    </label>
                    <select name="tags[]" id="tags" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 select2-multi" multiple="multiple">
                        @foreach( \App\Tag::all() as $tag )
                          <option value="{{ $tag->id }}">{{ $tag->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-4">
                    <input type="checkbox" name="featured_article" id="featured_article"> <span class="text-grey-darker text-sm font-bold">Featured</span>
                  </div>
                <div class="mb-4">
                    <img class="h-32 w-32 rounded-full mx-auto" src="{{ $article->featured_image }}">
                    <label class="block text-grey-darker text-sm font-bold mb-2" for="featured">
                      Featured Image
                    </label>
                    <input type="file" name="featured_image" id="featured_image">
                </div>
                <div class="flex items-center justify-between">
                  <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Update
                  </button>
                </div>
              </form>
              <p class="text-center text-grey text-xs">
                ©2019 Blog Test @ Mindvalley. All rights reserved.
              </p>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
  <script type="text/javascript">
		// $(".select2-multi").select2();
    $("#tags").select2();
	</script>
@endsection