<?php

/*
|--------------------------------------------------------------------------
| Web Routes - Test for @Mindvalley
|--------------------------------------------------------------------------
|
*/

// Pages ------------------------------------------------------------------
Route::get('/', 'PageController@index');
Route::get('/detail/{id}', 'PageController@getDetails');
Route::get('/stories', 'PageController@getStories')->name('get.stories');

// Article Resource ------------------------------------------------------
Route::resource('articles', 'ArticleController');
Route::get('featured', 'ArticleController@getRandomFeatured');
Route::get('/articles/tags/{tag}', 'ArticleController@getTagArticles')->name('tag.articles');

// Auth ------------------------------------------------------------------
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
