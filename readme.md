# Test Laravel Blog
- Blog uses Laravel 5.8, Vue 2.5, Tailwind CSS

## Installation Steps

1. Clone the repo and `cd` into it
1. `composer install`
1. Rename or copy `.env.example` file to `.env`
1. `php artisan key:generate`
1. Set your database credentials in your `.env` file
1. Set your `APP_URL` in your `.env` file
1. `php artisan migrate`
1. `php artisan db:seed`
1. `npm install`
1. `npm run dev`
1. `php artisan serve` or use Laravel Valet or Laravel Homestead
1. Visit `localhost:8000` in your browser
1. Create articles by get started new user registration or login with one of the seeded email with password as password
1. Test - phpunit or vendor/bin/phpunit tests/Feature/Http/Controllers/Api/ArticleControllerTest.php

---
Email me for any issues at [jamalniyaz@live.com](Mailto:jamalniyaz@live.com)