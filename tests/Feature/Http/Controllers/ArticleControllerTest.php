<?php

namespace Tests\Feature\Http\Controllers;

use App\Article;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleControllerTest extends TestCase
{
    /**
     *
     * @test can create an article
     */
    public function can_create_an_article()
    {        

        $article = factory(Article::class)->create();

        $response = $this->json('POST', '/articles', $article->toArray());

        $response->assertStatus(201);
    }

    /**
     * @test will it fail with 404 if not found
     */
    public function will_it_fail_with_404_if_article_not_found()
    {
        $response = $this->json('GET', 'articles/-1');
        return $response->assertStatus(404);
    }

    /**
     * @test can return an article
     */
    public function can_return_an_article()
    {
        $article = factory(Article::class)->create();

        $response = $this->json('GET', "articles/$article->id");
        $response->assertStatus(200);
    }

    /**
     * @test can update an article
     */
    public function can_update_an_article()
    {
        
        $article = factory(Article::class)->create();

        $response = $this->json('PUT', "articles/$article->id", [
            'title' => $article->title." updated",
            'short_content' => $article->short_content." updated",
            'long_content' => $article->long_content." updated"
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test will it fail with 404 if article to be updated not found
     */
    public function will_fail_with_404_if_article_to_be_updated_not_found()
    {
        $response = $this->json('PUT', "articles/-1");
        $response->assertStatus(404);
    }

    /**
     * @test will it fail with 404 if article to be deleted not found
     */

    public function will_fail_with_404_if_article_to_be_deleted_not_found()
    {
        $response = $this->json('DELETE', "articles/-1");
        $response->assertStatus(404);
    }

    /**
     * @test can delete an article
     */
    public function can_delete_an_article()
    {
        
        $article = factory(Article::class)->create();

        $response = $this->json('DELETE', "articles/$article->id");
        
        $response
                ->assertStatus(204)
                ->assertSee(null);
    }

    /**
     * @test can create and paginate collection of articles
     */
    public function can_create_and_get_paginated_response_of_articles()
    {
        $articles = factory(Article::class, 5)->create();
        $response = $this->json('GET', 'articles');
        $response->assertStatus(200);
    }
}
