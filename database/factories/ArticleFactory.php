<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'author_id' => rand(1, 3),
        'title' => $title,
        'slug' => \Str::slug($title),
        'short_content' => $faker->sentence,
        'long_content' => $faker->paragraph,
        'featured_image' => "https://via.placeholder.com/500x300",
        'article_image' => "https://via.placeholder.com/500x300",
        'status' => \Arr::random(['draft', 'published'])
    ];
});
