<?php

use App\Article;
use Illuminate\Database\Seeder;

class ArticleTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = Article::all(['id']);

        foreach ($articles as $key => $article) {
            \DB::table('article_tag')->insert([
                'article_id' => $article->id,
                'tag_id' => rand(1, 4),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

    }
}
