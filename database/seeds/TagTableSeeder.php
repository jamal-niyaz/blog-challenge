<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = ['programming', 'general', 'science', 'history', 'technology'];
        foreach ($tags as $tag) {
            \DB::table('tags')->insert([
                'title' => $tag
            ]);
        }
    }
}
